(function () {
  var listener;
  listener = function(request, sender, sendResponse) {
      browser.runtime.onMessage.removeListener(listener);
      window.location = request.url;
    }

    browser.runtime.onMessage.addListener(listener);
})();
